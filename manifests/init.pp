# This class is meant to be an example of how in module hiera data works.
class example (
  String $key_of_string,
  String $key_of_osfamily,
  String $key_of_local_default = "init.pp-default",
) {
  notify { 'test hiera common':
    message => "This is a message from '${key_of_string}' in the hiera in module data"
  }
  notify { 'test hiera osfamily':
    message => "This is a message from '${key_of_osfamily}', in the hiera in module data"
  }
  notify { 'test hiera local default':
    message => "This is a message from '${key_of_local_default}, in the hiera in module data"
  }

}
